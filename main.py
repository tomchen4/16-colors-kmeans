from imageio import imread, imwrite
import numpy as np
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from numpy.linalg import svd
import os

pixels = imread('Abstract256.jpg')
print(pixels.shape)
flattend = np.matrix([pixels[:,:,0].flatten(), pixels[:,:,1].flatten(), pixels[:,:,2].flatten()]).transpose()
print(flattend.shape)

clusters = KMeans(n_clusters = 16).fit(flattend)
print(clusters.cluster_centers_)
print(clusters.labels_)

compressed = np.zeros(pixels.shape)
for i in range(0, compressed.shape[0] - 1):
	for j in range(0, compressed.shape[1] - 1):
		compressed[i,j] = clusters.cluster_centers_[clusters.labels_[i*compressed.shape[1] + j]]
imwrite('compressed.jpg', compressed)
